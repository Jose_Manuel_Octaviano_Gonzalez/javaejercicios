import java.util.List;


public class FP_Functional_Exercises{
	
	public static void main(String [] args){
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

		List<String> courses = List.of("Spring", "Spring Boot", "API",
		"Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");
		//Exercise #1
		printEvenNumbersInListFunctional(numbers);
		System.out.println("");

		//Exercise #2
		FP_Functional_Exercises.printEvenCoursesInListFunctional(courses);
		System.out.println("");
		
		//Exercise #3		
		FP_Functional_Exercises.printEvenCourseInListFunctional(courses);
		System.out.println("");
		
		//Exercise #4

		FP_Functional_Exercises.printEvenCoursInListFunctional(courses);
		System.out.println("");

		//Exercise #5
		printSquaresOfEvenNumbersInListFunctional(numbers);
		System.out.println("");

		//Exercise #6
		FP_Functional_Exercises.printEvenCourInListFunctional(courses);
		System.out.println("");


		//System.out.println("\n");
		
	}

	private static void print(int number){
		System.out.print(number + ", ");
	}

	/*private static boolean isEven(int number){
		return (number % 2 != 0);
	}*/
	
	private static void print(String courses){
		System.out.print("Course: " + courses +"\n");
	}

	/*private static boolean isEvenC(String course){
		return ();
	}*/
	
	
////Execise #1///////
	private static void printEvenNumbersInListFunctional(List<Integer> numbers){
		System.out.println("Exercise 1: Números Impares ");
		numbers.stream()					
				.filter(number -> number % 2 != 0)  
				.forEach(FP_Functional_Exercises::print);  
		System.out.println("");
	}

/////Exercise #2/////

	private static void printEvenCoursesInListFunctional(List<String> courses) {
		
		System.out.println("Exercise 2: Todos los cursos individualmente");
			courses.stream()                                
				.forEach(FP_Functional_Exercises::print); 
		System.out.println("");
	}

////Exercise #3
	private static void printEvenCourseInListFunctional(List<String> courses) {
		
		System.out.println("Exercise 3: Cursos que contengan la palabra Spring");
		courses.stream()
				.filter((s) -> s.contains("Spring"))
				.forEach(FP_Functional_Exercises::print);
		System.out.println("");
	}

///Exercise #4
//palabras.stream().allMatch(s -> s.length() >= 4);

	private static void printEvenCoursInListFunctional(List<String> courses) {
		
		System.out.println("Exercise 4: Cursos que contengan al menos 4 letras ");
		courses.stream()
				.filter(s -> s.length() >= 4)
				//.allMatch(s -> s.length() >= 4)
				.forEach(FP_Functional_Exercises::print);
		System.out.println("");
	}


///Exercise #5
	private static void printSquaresOfEvenNumbersInListFunctional(List<Integer> numbers){
		System.out.println("Exercise 5: Los cubos de números impares");		
		numbers.stream()                            
				.filter(number -> number % 2 != 0)  
				.map(number -> number * number *number)     
				.forEach(FP_Functional_Exercises::print); 
		System.out.println("");	
	}

///Exercise #6 solo cuenta cuantas palabras estan escritas
	private static void printEvenCourInListFunctional(List<String> courses) {
		System.out.println("Exercise 6:  Número de letras qué contiene cada curso");
		courses.stream()
				.map(course -> course + " = " + course.length())
				.forEach(FP_Functional_Exercises::print);  
		System.out.println("");
	}





}
